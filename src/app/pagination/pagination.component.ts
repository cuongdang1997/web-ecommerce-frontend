import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { PagingService } from '../services/paging.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit, OnChanges {
  // pager object
  pager: any = {};
  totalItemsOld = -1;
  currentPage: number;
  @Input() totalItems = 0;
  @Input() changedResultSearch: boolean;
  @Output() changePage = new EventEmitter();
  constructor(private pagingService: PagingService) {
  }

  ngOnChanges() {
      this.currentPage = this.pager.currentPage;
      if (this.totalItemsOld === this.totalItems - 1) {
              this.currentPage = Math.ceil((this.totalItems) / 10) - 1;
      } else if ((this.totalItemsOld === this.totalItems + 1) && ((this.totalItems % 10) === 0) && (this.totalItems !== 0)
                  && this.currentPage === this.pager.totalPages - 1 && this.pager.totalPages > 1 ) {
              --this.currentPage;
      } else if (this.totalItemsOld > this.totalItems + 1) {
          this.currentPage = 0;
      }
      this.totalItemsOld = this.totalItems;
      this.setPage(this.currentPage);
  }

  ngOnInit() {
  }

  setPage(page: number) {
      if (page < 0 || page > this.pager.totalPages) {
          return;
      }
      this.pager = this.pagingService.getPager(this.totalItems, page);
      this.changePage.emit(this.pager);
  }
}
