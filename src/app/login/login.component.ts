import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import {TokenStorageService } from '../services/token-storage.service';

import { AuthLoginInfo } from '../model/login-info';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
    private loginInfo: AuthLoginInfo;
    constructor(
                private myRoute: Router,
                private authService: AuthService,
                private tokenStorage: TokenStorageService ) {
    }
    ngOnInit() {
    }

    onSubmit() {
        this.loginInfo = new AuthLoginInfo(
          this.form.username,
          this.form.password);
        this.authService.attemptAuth(this.loginInfo).subscribe(
          data => {
            this.tokenStorage.saveToken(data.accessToken);
            this.tokenStorage.saveUsername(data.username);
            this.tokenStorage.saveAuthorities(data.authorities);
            this.isLoginFailed = false;
            this.isLoggedIn = true;
            this.roles = this.tokenStorage.getAuthorities();
            this.myRoute.navigate(['dashboard']);
          }, error => {
            console.log(error);
            this.errorMessage = error.error.message;
            this.isLoginFailed = true;
          }
        );
    }


}
