import { Component } from '@angular/core';
import {TokenStorageService } from './services/token-storage.service';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Admin - ShopOnline';

  constructor(public tokenStorage: TokenStorageService) {
  }
}
