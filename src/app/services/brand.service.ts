import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Brand} from '../model/brand';
import {Urlconfig} from '../urlconfig';

@Injectable({
    providedIn: 'root'
})
export class BrandService {
    constructor(private http: HttpClient) {
    }

    /**
     * get BrandList
     * '@returns {Observable<Brand[]>}
     */
    getBrands(): Observable<Brand[]> {
        return this.http.get(Urlconfig.BRAND_URL).pipe(
            map(data => data as Brand[])
        );
    }
}