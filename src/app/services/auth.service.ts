import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtResponse } from '../model/jwt-response';
import { AuthLoginInfo } from '../model/login-info';
import {Observable} from 'rxjs';

const TOKEN_KEY = 'AuthToken';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    private loginUrl = 'http://localhost:8080/api/auth/signin';

  constructor(private myRoute: Router, private http: HttpClient) {
  }

    /*attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
        return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
    }

    sendToken(token: string) {
        localStorage.setItem('LoggedInUser', token);
    }
    getToken() {
        return localStorage.getItem('LoggedInUser');
    }
    isLoggedIn() {
        return this.getToken() !== null;
    }
    logout() {
        localStorage.removeItem('LoggedInUser');
        this.myRoute.navigate(['Login']);
    }*/
    attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
        return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
    }
    /*signUp(info: SignUpInfo): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions);
    }*/
}
