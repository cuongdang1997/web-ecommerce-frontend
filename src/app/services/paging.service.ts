import { Injectable } from '@angular/core';
import * as _ from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class PagingService {

    /**
     * @param totalItems : Total items to be listed
     * @param currentPage : Current page number ( Pages starting from 1 not 0)
     * @param pageSize : The number of items in the page
     * @param totalPageLinkButtons : The number of total page link buttons
     * @returns '{{
   * startIndex: number,
   * endIndex: number,
   * totalPageLinkButtons: number,
   * totalItems: number,
   * currentPage: number,
   * pageSize: number,
   * totalPages: number,
   * pages: (Observable<number>|any)
   * }}
     */
    getPager(totalItems: number, currentPage: number = 0, pageSize: number = 10) {
        // calculate total pages
        const totalPages = Math.ceil(totalItems / pageSize);
        let startPage: number, endPage: number;
        if (totalPages <= 5) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= 3) {
                startPage = 1;
                endPage = 5;
            } else if (currentPage + 1 >= totalPages) {
                startPage = totalPages - 4;
                endPage = totalPages;
            } else {
                startPage = currentPage - 2;
                endPage = currentPage + 2;
            }
        }

        // create an array of pages to ng-repeat in the pager control
        const pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            pages: pages
        };
    }

}
