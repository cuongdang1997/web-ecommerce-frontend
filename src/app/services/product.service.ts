import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Product} from '../model/product';
import {Urlconfig} from '../urlconfig';

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    constructor(private http: HttpClient) {
    }

    /**
     * get ProductList
     * '@returns {Observable<Product[]>}
     */
    getProducts(): Observable<Product[]> {
        return this.http.get(Urlconfig.PRODUCT_URL).pipe(
            map(data => data as Product[])
        );
    }

    /**
     * get Product by id
     * '@param {number} id
     * '@returns {Observable<Product>}
     */
    getProduct(id: number): Observable<Product> {
        return this.http.get<Product>(`${Urlconfig.PRODUCT_URL}/${id}`);
    }

    /**
     * Create Product
     * '@param {Product} product
     * '@returns {Observable<Product>}
     */
    createProduct(product: Product): Observable<Product> {
        return this.http.post<Product>(Urlconfig.PRODUCT_URL, product, {headers: Urlconfig.HTTP_HEADERS});
    }

    /**
     * update product
     * '@param {Product} product
     * '@returns {Observable<Product>}
     */
    updateProduct(product: Product): Observable<Product> {
        return this.http.put<Product>(Urlconfig.PRODUCT_URL, product, {headers: Urlconfig.HTTP_HEADERS});
    }

    /**
     * Delete Brand by id
     * '@param {number} id
     * '@returns {Observable<Brand>}
     */
    deleteProduct(idProduct: string): Observable<Product> {
        return this.http.delete<Product>(`${Urlconfig.PRODUCT_URL}/${idProduct}`, {headers: Urlconfig.HTTP_HEADERS});
    }

    /**
     * count all
     */
    countAll(): Observable<number> {
        return this.http.get(`${Urlconfig.PRODUCT_URL}/${'countAll'}`).pipe(
            map(data => data as number)
        );
    }

    /**
     * get brand per page
     * '@param {number} page
     * '@returns {Observable<Brand[]>}
     */
    getProductsPerPage(page: number): Observable<Product[]> {
        return this.http.get(`${Urlconfig.PRODUCT_URL}/${page}/10`).pipe(
            map(data => data as Product[])
        );
    }
}