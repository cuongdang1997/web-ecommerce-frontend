import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { Urlconfig } from '../urlconfig';
import { Order } from '../model/order';

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    constructor(private http: HttpClient) {
    }

    getOrderPerpage(page: number): Observable<Order[]> {
        return this.http.get(`${Urlconfig.ORDER_URL}/${page}/50`).pipe(
            map(data => data as Order[])
        );
    }

    confirmOrder(orderId: string): Observable<Order> {
        return this.http.get(`${Urlconfig.ORDER_URL}/Confirm/${orderId}`).pipe(
            map(data => data as Order)
        );
    }
}
