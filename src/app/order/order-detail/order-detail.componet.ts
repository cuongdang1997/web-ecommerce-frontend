import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/model/order';
import { Urlconfig } from 'src/app/urlconfig';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.componet.html'
})
export class OrderDetailComponent implements OnInit {
  BASE_URL = Urlconfig.BASE_URL;
  @Input()
    set setDataForOrderdetail(order: Order) {
        if (order != null) {
            this.order = order;
        }
    }

  order: Order;
  constructor() { }

  ngOnInit() {
  }

}
