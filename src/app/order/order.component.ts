import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { ModalService } from '../services/modal.service';
import { ConfirmationDialogService } from '../services/confirmation-dialog.service';
import { NotificationService } from '../services/notification.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  orderDetail: Order = null;
  idOrderModal = 'modalOrderdetail';
  orders: Order[];
  constructor(private orderService: OrderService,
    private modalService: ModalService,
    private confirmDialogService: ConfirmationDialogService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.orderService.getOrderPerpage(0).subscribe(
      data => {
        this.orders = data;
      });
  }

    /**
     * Event click on edit button
     * '@param {Product} product
     */
    onViewOrder(order: Order) {
      this.orderDetail = order;
      this.openModal(this.idOrderModal);
    }

  /**
     * Open Modal By Ids
     * '@param {string} id
     */
    openModal(idModal: string) {
      this.modalService.open(idModal);
    }

    /**
     * Close Modal By Id
     * '@param idModal
     */
    closeModal(idModal: string) {
      this.modalService.close(idModal);
    }

    confirmOrder(orderId: string) {
      this.confirmDialogService.confirm('Xác nhận', 'Bạn có muốn xác nhận đơn hàng: '
                                        + orderId + ' đã thanh toán hay không ?')
          .then((confirmed) => {
              if (confirmed) {
                  this.orderService.confirmOrder(orderId).subscribe(data => {
                    const index = this.orders.findIndex(item => item.orderID === data.orderID);
                    this.orders.splice(index, 1, data);
                      swal({
                        title: 'Thành công',
                        text: 'Đã xác nhận thanh toán!',
                        timer: 1500,
                    });
                  });
              }
          });
    }
}
