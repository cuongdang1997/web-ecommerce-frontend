import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Product} from '../../model/product';
import {BrandService} from '../../services/brand.service';
import {Brand} from '../../model/brand';
import {Urlconfig} from '../../urlconfig';
import { Category } from 'src/app/model/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html'
})
export class AddProductComponent implements OnInit {
    origins = ['Việt Nam', 'Mỹ', 'Ấn độ', 'Trung Quốc', 'Hà Lan', 'Thụy điển', 'Canada']; 
    brandIDSelected: string;
    categoryIDSelected: string;
    avatar_src = '';
    selectFile: File = null;
    brands: Brand[];
    categorys: Category[];
    @Input() buttonName: string;
    @Input()
    set setDataForProductForm(product: Product) {
        if (product != null) {
            this.productForm.setValue(product);
            this.avatar_src = Urlconfig.BASE_URL + '/' + product.imageUrl;
            this.brandIDSelected = product.brand.brandID;
            this.categoryIDSelected = product.category.categoryID;
        }
    }
    @Output() addProductRequest = new EventEmitter<Product>();
    @Output() editProductRequest = new EventEmitter<Product>();

    /**
     * Product Form
     *' @type {FormGroup}
     */
    productForm = new FormGroup({
        productID: new FormControl(null),
        productName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.nullValidator]),
        quantity: new FormControl('', [Validators.required]),
        origin: new FormControl('', [Validators.required]),
        openingForSale: new FormControl(Date(), [Validators.required]),
        imageUrl: new FormControl(null, [Validators.required]),
        price: new FormControl('', [Validators.required]),
        brand: new FormControl(null, [Validators.required]),
        category: new FormControl(null, [Validators.required]),
        changedImageUrl: new FormControl(false),
    });

    constructor(private brandService: BrandService, private categoryService: CategoryService) {
    }

    ngOnInit() {
        this.brandService.getBrands().subscribe(
            data => {
                this.brands = data;
            });
        this.categoryService.getCategorys().subscribe(
            data => {
                this.categorys = data;
            });
    }

    /**
     * selected file
     * '@param event
     */
    onFileSelected(event) {
        if (event.target.files && event.target.files[0]) {
            const reader: FileReader = new FileReader();
            this.selectFile = event.target.files[0];
            reader.readAsDataURL(this.selectFile);
            reader.onload = (readerEvt: Event) => {
                this.avatar_src = reader.result.toString();
            };
            const readerBase64 = new FileReader();
            readerBase64.readAsBinaryString(this.selectFile);
            readerBase64.onload = (readerEvt: Event) => {
                this.productForm.controls['imageUrl'].setValue(btoa(readerBase64.result.toString()));
            };
        }
    }

    /**
     * On submit
     */
    onSubmit() {
        if (this.productForm.valid) {
            if (!this.productForm.controls['productID'].value) {
                this.addProductRequest.emit(this.productForm.value);
            } else {
                if (this.selectFile) {
                    this.productForm.controls['changedImageUrl'].setValue(true);
                }
                this.editProductRequest.emit(this.productForm.value);
            }
            this.reset();
        }
    }

    /**
     * reset form
     */
    reset() {
        this.productForm.reset();
        this.selectFile = null;
        this.avatar_src = '';
    }
}
