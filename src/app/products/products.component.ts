import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../model/product';
import { ModalService } from '../services/modal.service';
import { ConfirmationDialogService } from '../services/confirmation-dialog.service';
import {Urlconfig} from '../urlconfig';
import { NotificationService } from '../services/notification.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  BASE_URL = Urlconfig.BASE_URL;
  idAddModal = 'modalAddProduct';
  idEditModal = 'modalEditProduct';
  changedResultSearch = false;
  totalItems = 0;
  currentPage = 0;
  products: Product[];
  product: Product = null;
  model = 1;
  checkboxModel = {
   left: true,
   middle: false,
   right: false
 };
  constructor(private productsService:  ProductService, private modalService: ModalService,
    private confirmDialogService: ConfirmationDialogService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.countAll();
  }

   /**
     * Open Modal By Ids
     * '@param {string} id
     */
  openModal(idModal: string) {
    this.modalService.open(idModal);
  }

  /**
   * Close Modal By Id
   * '@param idModal
   */
  closeModal(idModal: string) {
    this.modalService.close(idModal);
  }

  /**
     * Event click on edit button
     * '@param {Product} product
     */
  onEditProduct(product: Product) {
    this.product = product;
    this.openModal(this.idEditModal);
  }

  /**
     * Add Product
     * '@param product
     */
  addProduct(product: Product) {
    this.productsService.createProduct(product)
      .subscribe(data => {
          this.closeModal(this.idAddModal);
          this.products.push(data);
          swal({
            title: 'Thành công!',
            text: 'Thêm mới sản phẩm thành công!',
            timer: 1500,
        });
      });
  }

  /**
     * Edit Product
     * '@param product
     */
  editProduct(product: Product) {
      this.productsService.updateProduct(product)
          .subscribe(data => {
              const index = this.products.findIndex(item => item.productID === data.productID);
              this.products.splice(index, 1, data);
              this.closeModal(this.idEditModal);
              swal({
                title: 'Thành công!',
                text: 'Cập nhật thông tin sản phẩm thành công!',
                timer: 1500,
            });
          });
  }

  /**
   * Delete Product
   * '@param {Product} product
   */
  deleteProduct(product: Product): void {
      const index = this.products.findIndex(item => item.productID === product.productID);
      this.confirmDialogService.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm này không?')
          .then((confirmed) => {
              if (confirmed) {
                  this.productsService.deleteProduct(product.productID).subscribe(data => {
                      /* if (!this.isSearching) {
                          --this.totalItems;
                      } else { */
                      this.products.splice(index, 1);
                      swal({
                        title: 'Thành công!',
                        text: 'Đã xóa sản phẩm!',
                        timer: 1500,
                    });
                  });
              }
          });
  }

  /**
     * Event Page Changed
     * '@param event
     */
  pageChanged(event) {
    this.currentPage = event.currentPage;
        this.productsService.getProductsPerPage(this.currentPage).subscribe(
            data => {
                this.products = data;
            });
  }

  /**
     * Count All product
     */
  countAll() {
    this.productsService.countAll().subscribe(
        data => {
            this.totalItems = data;
        });
  }
}
