import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {TokenStorageService } from '../services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private tokenStorage: TokenStorageService, private myRoute: Router ) {
    }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.tokenStorage.getToken()) {
        const index = this.tokenStorage.getAuthorities().findIndex(item => item === 'ROLE_ADMIN');
        if (index < 0) {
          this.tokenStorage.signOut();
        }
        return index >= 0;
      } else {
          this.myRoute.navigate(['login']);
          return false;
      }
  }
}
