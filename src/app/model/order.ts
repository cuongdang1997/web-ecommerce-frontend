import { OrderDetail } from './orderdetail';

export class Order {
    orderID: string;
    orderDetailsList: OrderDetail[];
    orderDiscount: string;
    recipientName: string;
    shipName: string;
    shipAddress: string;
    zip: string;
    city: string;
    district: string;
    village: string;
    phone: string;
    email: string;
    orderDate: string;
    shipped: number;
    confirmed: 0;
    paid: 0;
    payment_method: string;
    purchaseAmount: number;
}
