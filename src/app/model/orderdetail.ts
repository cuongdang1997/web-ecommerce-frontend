import { Product } from './product';

export class OrderDetail {
    orderdetailID: number;
    product: Product;
    amount: number;
    price: number;
}
