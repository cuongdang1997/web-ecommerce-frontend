export class Brand {
    brandID: string;
    brandName: string;
    logo: string;
    products: string;
    changedLogo: boolean;
}