import { Brand } from './brand';
import { Category } from './category';

export class Product {
    productID: string;
    productName: string;
    quantity: number;
    origin: string;
    openingForSale: string;
    imageUrl: string;
    price: number;
    brand: Brand;
    category: Category;
    changedImageUrl: boolean;
}