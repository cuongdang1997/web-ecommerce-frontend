import {Component, OnInit, ElementRef, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import {ModalService} from '../../../services/modal.service';

@Component({
    selector: 'app-modal-dialog',
    templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit, OnDestroy {

    @Input() id: string;
    @Input() title: string;
    @Output() closeModalRequest = new EventEmitter<string>();
    private element: any;

    constructor(private modalService: ModalService, private el: ElementRef) {
        this.element = el.nativeElement;
    }

    ngOnInit(): void {
        const modal = this;

        // ensure id attribute exists
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        // close modal on background click
        this.element.addEventListener('click', function (e: any) {
            if (e.target.className === 'modal') {
                modal.close();
            }
        });

        // add self (this modal instance) to the modal service so it's accessible from controllers
        this.modalService.add(this);
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    // open modal
    open(): void {
        this.element.style.display = 'block';
        document.body.classList.add('modal-open');
    }

    // close modal
    close(): void {
        this.element.style.display = 'none';
        document.body.classList.remove('modal-open');
    }

    /**
     * close Modal
     */
    onCloseModal() {
        this.closeModalRequest.emit(this.id);
    }
}