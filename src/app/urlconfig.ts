import { HttpHeaders } from '@angular/common/http';

export class Urlconfig {
    static HTTP_HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
    static BASE_URL = 'http://localhost:8080';
    static BRAND_URL = `${Urlconfig.BASE_URL}/api/brand`;
    static CATEGORY_URL = `${Urlconfig.BASE_URL}/api/category`;
    static PRODUCT_URL = `${Urlconfig.BASE_URL}/api/product`;
    static ORDER_URL = `${Urlconfig.BASE_URL}/api/order`;
}
